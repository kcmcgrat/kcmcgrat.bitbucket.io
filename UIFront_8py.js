var UIFront_8py =
[
    [ "getData", "UIFront_8py.html#afcb25bb81b9cd65f90c511f79b54da70", null ],
    [ "kb_cb", "UIFront_8py.html#a45e1f1106aad5c2aeebbab937703d3cf", null ],
    [ "baudrate", "UIFront_8py.html#ad67edefbea5ceb5534b2786e575f0730", null ],
    [ "callback", "UIFront_8py.html#ac32f9f594ce81fd0792445cb4b777b7c", null ],
    [ "cur", "UIFront_8py.html#a05e6747e2cbc2c0c812f0d3723dddee7", null ],
    [ "dataString", "UIFront_8py.html#a4f622ee250776ff99df2e9a60e6f9f2a", null ],
    [ "delta", "UIFront_8py.html#ae8e60a5a75f69077373dfe6b70af5d55", null ],
    [ "file", "UIFront_8py.html#ac9b89b1e3a67a53a1fe46ffb85f2be6b", null ],
    [ "kb_cb", "UIFront_8py.html#af9b2968a7cc02edf0f98bac6eb5027ef", null ],
    [ "last_key", "UIFront_8py.html#a0b36daba4254cf763f1724fb71392451", null ],
    [ "port", "UIFront_8py.html#af7a4c5a93195c99788ad0bc50cd1cba3", null ],
    [ "position", "UIFront_8py.html#acbeebf1cf92adc96edddbde4398b5654", null ],
    [ "splitCur", "UIFront_8py.html#afba48b21e073780bc10cba2e438ff1f6", null ],
    [ "stripCur", "UIFront_8py.html#a11c77b5679fa57cb55d723309ee3f3be", null ],
    [ "suppress", "UIFront_8py.html#a44b062e941c5e03def446f4723bc7370", null ],
    [ "timeout", "UIFront_8py.html#ac52f9ff3841612f6e63e0aa0b4b02e26", null ],
    [ "times", "UIFront_8py.html#a813a3077e09872f05e3f8c09e6363f6d", null ],
    [ "user_in", "UIFront_8py.html#a3ac615c53da38290fe072c9bbcdb328a", null ],
    [ "vals", "UIFront_8py.html#a671b4852aeac3d28ecb580a94aae6a34", null ]
];