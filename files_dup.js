var files_dup =
[
    [ "closedLoopClass.py", "closedLoopClass_8py.html", [
      [ "ClosedLoop", "classclosedLoopClass_1_1ClosedLoop.html", "classclosedLoopClass_1_1ClosedLoop" ]
    ] ],
    [ "dataCollector.py", "dataCollector_8py.html", [
      [ "Collect", "classdataCollector_1_1Collect.html", "classdataCollector_1_1Collect" ]
    ] ],
    [ "ElevatorFSM.py", "ElevatorFSM_8py.html", "ElevatorFSM_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "encoderTask.py", "encoderTask_8py.html", [
      [ "EncoderTask", "classencoderTask_1_1EncoderTask.html", "classencoderTask_1_1EncoderTask" ]
    ] ],
    [ "fibonacci.py", "fibonacci_8py.html", "fibonacci_8py" ],
    [ "Lab02.py", "Lab02_8py.html", "Lab02_8py" ],
    [ "lab03main.py", "lab03main_8py.html", "lab03main_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "morse.py", "morse_8py.html", [
      [ "Morse", "classmorse_1_1Morse.html", "classmorse_1_1Morse" ]
    ] ],
    [ "motorDriver.py", "motorDriver_8py.html", [
      [ "MotorDriver", "classmotorDriver_1_1MotorDriver.html", "classmotorDriver_1_1MotorDriver" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "Shares", "classshares_1_1Shares.html", "classshares_1_1Shares" ]
    ] ],
    [ "UIFront.py", "UIFront_8py.html", "UIFront_8py" ]
];