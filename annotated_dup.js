var annotated_dup =
[
    [ "closedLoopClass", null, [
      [ "ClosedLoop", "classclosedLoopClass_1_1ClosedLoop.html", "classclosedLoopClass_1_1ClosedLoop" ]
    ] ],
    [ "dataCollector", null, [
      [ "Collect", "classdataCollector_1_1Collect.html", "classdataCollector_1_1Collect" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "encoderTask", null, [
      [ "EncoderTask", "classencoderTask_1_1EncoderTask.html", "classencoderTask_1_1EncoderTask" ]
    ] ],
    [ "morse", null, [
      [ "Morse", "classmorse_1_1Morse.html", "classmorse_1_1Morse" ]
    ] ],
    [ "motorDriver", null, [
      [ "MotorDriver", "classmotorDriver_1_1MotorDriver.html", "classmotorDriver_1_1MotorDriver" ]
    ] ],
    [ "shares", null, [
      [ "Shares", "classshares_1_1Shares.html", "classshares_1_1Shares" ]
    ] ]
];